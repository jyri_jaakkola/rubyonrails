class CreateExercises < ActiveRecord::Migration
  def change
    create_table :exercises do |t|
      t.string :title
      t.text :body
      t.string :url
      t.references :calendar, index: true

      t.timestamps
    end
  end
end
