class CreateCalendars < ActiveRecord::Migration
  def change
    create_table :calendars do |t|
      t.date :date
      t.text :desc
      t.text :user

      t.timestamps
    end
  end
end
