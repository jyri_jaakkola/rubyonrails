class WelcomeController < ApplicationController
  before_filter :save_login_state, :only => [:login, :login_attempt]

  def index
  end

  def login
  end

  def login_attempt
  	authorized_user = User.authenticate(params[:username], params[:login_password])
    if authorized_user
      session[:user_id] = authorized_user.id
      flash[:notice] = nil
      redirect_to :controller => 'calendars', :action => 'index'
    else
      flash[:notice] = "Invalid Username or Password"
      flash[:color] = "invalid"
      render "login"
    end
  end

  def logout
  	session[:user_id] = nil
    flash[:notice] = nil
    redirect_to '/'
  end

  def home
  end
end
