require 'date'

class CalendarsController < ApplicationController
  before_filter :authenticate_user

  attr_accessor :day
  attr_accessor :week
  attr_accessor :started

  def index
  	@started = 0
  	@week = 1
  	if (params[:year].to_i > 0)
  		@time = Date.new(params[:year].to_i, params[:month].to_i, 1)
  		if (params[:direction] == 'prev')
  			@time = @time - 1.month
  		elsif (params[:direction] == 'next')
  			@time = @time + 1.month
  		end
  	else
  		@time = DateTime.now
  	end

  	@calendars = Calendar.where(user: session[:user_id].to_s).where("strftime('%m', date)+0 = ?", @time.month).where("strftime('%Y', date)+0 = ?", @time.year)
  	@exes = []
  	@calendars.each do |calendar|
  		@exes[calendar.date.day] = calendar
  	end

  	@days_now = Calendar.days_in_month(@time.month, @time.year)
  	@day = Date.new(@time.year, @time.month, 1).wday
  	if @day == 0
  		@day = 7
  	end
  end

  def show
  	@calendar = Calendar.find(params[:id])
  end

  def new
  	@day = params[:day].to_i
  	@month = params[:month].to_i
  	@year = params[:year].to_i
  	@date = Date.new(@year, @month, @day)

  	@calendar = Calendar.new
  end

  def edit
  	@calendar = Calendar.find(params[:id])
  end

  def update
  	@calendar = Calendar.find(params[:id])

  	if @calendar.update(calendar_params)
  		redirect_to @calendar
  	else
  		render 'edit'
  	end
  end

  def create
  	@calendar = Calendar.new(calendar_params)
	@time = @calendar.date

  	if @calendar.save
  		redirect_to calendars_path(:month => @time.month, :year => @time.year)
  	else
  		render 'new'
  	end
  end

  def destroy
  	@calendar = Calendar.find(params[:id])
  	@calendar.destroy
  	redirect_to calendars_path
  end

  private
	  def calendar_params
	  	params.require(:calendar).permit(:date, :user, :desc)
	  end
end
