class ExercisesController < ApplicationController

	def create
		@calendar = Calendar.find(params[:calendar_id])
		@exercise = @calendar.exercises.create(exercise_params)
		redirect_to calendar_path(@calendar)
	end

	def edit
		@calendar = Calendar.find(params[:calendar_id])
		@exercise = @calendar.exercises.find(params[:id])
	end

	def update
		@calendar = Calendar.find(params[:calendar_id])
		@exercise = @calendar.exercises.find(params[:id])

		if @exercise.update(exercise_params)
			redirect_to @calendar
		else
			render 'edit'
		end
	end

	def destroy
		@calendar = Calendar.find(params[:calendar_id])
		@exercise = @calendar.exercises.find(params[:id])
		@exercise.destroy
		redirect_to calendar_path(@calendar)
	end

	private
		def exercise_params
			params.require(:exercise).permit(:title, :body, :url)
		end

end
