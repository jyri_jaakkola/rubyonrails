class Calendar < ActiveRecord::Base
  has_many :exercises

  def self.days_in_month(month, year)
    case month
      when 1, 3, 5, 7, 8, 10, 12
      	return 31
      when 4, 6, 9, 11
      	return 30
      when 2
      	if Calendar.leap_year(year) == 1
      		return 29
      	else
      		return 28
      	end
    end
  end

  def self.leap_year(year)
    if year % 400 == 0
      return 1
    elseif year % 100 == 0
      return 0
    elseif year % 4 == 0
      return 1
    else
      return 0
    end
  end
end
