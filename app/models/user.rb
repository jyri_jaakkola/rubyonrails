class User < ActiveRecord::Base
	validates :username, :presence => true, :uniqueness => true, :length => { :in => 4..20 }
	validates :password, :confirmation => true

	def self.authenticate(username="", login_password="")
      user = User.find_by_username(username)

      if user && user.match_password(user.password, login_password)
      	return user
      else
      	return false
      end
	end

	def match_password(password, login_password)
      if password == login_password
		return true
	  else
		false
	  end
	end
end
